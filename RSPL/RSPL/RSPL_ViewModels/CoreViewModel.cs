﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Core.Logging;
using Core.Views;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using RSPL;

namespace Core.ViewModels
{
    // navigation version
    public abstract partial class CoreViewModel
    {
        public virtual void ViewDisappearing()
        {
            Logger.Trace($" Page Disppearing => {this.GetType().Name}");

        }

        public virtual void ViewAppeared()
        {
            Logger.Trace($"Opening Page => {this.GetType().Name}");
        }

        public virtual void ViewAppearing()
        {
           
        }

        public virtual void PopupClosed(Rg.Plugins.Popup.Pages.PopupPage page)
        {
            Logger.Trace($" Popup Closed => {this.GetType().Name}");
        }
        void toggleMenu()
        {
            RSPL.App.Instance.ToggleMenu();
        }
        public NavigationService NavService { get => NavigationService.Instance; }

        public CorePage.Orientation DeviceOrientation
        {
            get => GetProperty<CorePage.Orientation>();
            set => SetProperty(value, HandleOrientationChanged);
        }
        public bool IsAnalyticsEnabled { get; set; }
        public static bool Isbeneficiary { get; set; }
        public static bool firsttime = true;
        public static int OnboardingSelectedMembershipType { get; set; }
        public static bool AppInQAMode { get; set; } = false;
        public static bool AppInDevMode { get; set; } = false;
        public bool IsVisibleShareAndEarnButton => true;
        public string BuyingPower { get => GetProperty<string>(); set => SetProperty(value); }
        public List<string> AccountTypeList { get => GetProperty<List<string>>(); set => SetProperty(value); }
        public bool IsAccountList { get => GetProperty<bool>(); set => SetProperty(value); }
        // public string SelectedAccount { get => GetProperty<string>(); set => SetProperty(value, onSelectAccount); }

        public bool IsEnableMultiNational { get; set; }
        public ICommand ToggleMenuCommand => GetCommand(toggleMenu);
        protected virtual void HandleOrientationChanged(CorePage.Orientation arg1, CorePage.Orientation arg2)
        {

        }

        public async void RefreshCurrentData()
        {
           
                
        }
        public virtual void SetNavigatedFrom(string fromVMName)
        {
            if (fromVMName.HasValue())
            {
                AddAnalytics("Navigated_From", fromVMName);
            }
        }

        protected void AddAnalytics(string key, string value)
        {
            if (AnalyticsAdditionalFields == null)
            {
                AnalyticsAdditionalFields = new Dictionary<string, string>();
            }
            if (AnalyticsAdditionalFields.ContainsKey(key))
                AnalyticsAdditionalFields[key] = value;
            else
                AnalyticsAdditionalFields.Add(key, value);
        }


        //public virtual async void onSelectAccount(string arg1, string arg2)
      //  {
          //  if (arg2 != null)
           // {
             //   if (arg2 == "Add Account")
              //  {
                    // await PopupNavigation.Instance.PushAsync(new Views.TextMessagePopup());
                //    SelectedAccount = arg1;
                 //   return;
                    // App.ProcessNotification(Constants.AddTradingAccountText);
              //  }
           // }
       // }


        protected IDictionary<string, string> AnalyticsAdditionalFields = null;
        public string SelectedAccountNumber { get => GetProperty<string>(); set => SetProperty(value); }
        public ICommand CloseCommand => GetCommand(closeModal);
        public ICommand CloseModelCommand => GetCommand(closeModal);

        protected async virtual void closeModal()
        {
            if (NavigationService.HasModal)
                await NavService.CloseModal();
            else
                await NavService.PopToRootAsync();
        }

    }

    public abstract partial class CoreViewModel : SimpleViewModel
    {
        //bool firstShowing = true;
        public bool IsAnalyticsRecordFirstEnabled { get; set; } = true;
   

        public CoreViewModel()
        {
           
           
        }

        //public async Task RefreshAppLocalData(string userId, int tradingAccountId)
        //{
        //    using (var dlg = Dialogs.Loading("Logging In..."))
        //    {
        //        await Task.Yield();
        //        //await Task.Delay(3000);

        //        var referralInfo = await ApiClient.Instance.GetReferralInformation(userId);
        //        if (referralInfo.Success && referralInfo.Data != null)
        //            App.Instance.User.ReferralCount = referralInfo.Data.Count.GetValueOrDefault();

        //        var acctResponse = await ApiClient.Instance.GetAccountInformation(tradingAccountId);
        //        if (acctResponse.Success && acctResponse.Data != null)
        //        {
        //            App.Instance.User.UpdateFrom(acctResponse.Data.Data);
        //        }

        //        var MembershipResponse = await ApiClient.Instance.GetPromoMembership(userId);
        //        if (MembershipResponse.Success && MembershipResponse.Data != null)
        //        {
        //            App.Instance.User.UpdateMembershipFrom(MembershipResponse.Data);
        //        }
        //        await App.Instance.User.UpdateBankAccounts();

        //        //await CheckReferralLink();

        //    }
        //}


       // protected Acr.UserDialogs.IUserDialogs Dialogs { get => Acr.UserDialogs.UserDialogs.Instance; }

        [JsonIgnore]
        public ICommand GoBackCommand => GetCommand(goBack);

        public ICommand ClosePopUp => GetCommand(closePopup);

        public async void closePopup()
        {
            await PopupNavigation.Instance.PopAsync();
        }
        public virtual async void goBack()
        {

            if (NavigationService.HasModal)
                await NavService.PopModal();
            else
                await NavigationService.Instance.PopAsync();


            if (IsAnalyticsEnabled)
            {

                Dictionary<string, string> dictTrackValues = null;
                if (propertyDictionary != null && propertyDictionary.Count > 0)
                {
                    dictTrackValues = new Dictionary<string, string>();
                    Dictionary<string, object> dataCollection = base.propertyDictionary.Where(x => x.Value != null).ToDictionary(y => y.Key, y => y.Value);
                    foreach (var item in dataCollection)
                    {
                        dictTrackValues.Add(item.Key, item.Value.ToString());
                    }
                }

             //   AppAnalyticsServices.AppEvent(this.GetType().Name, dictTrackValues);
              //  AppFacebookAnalyticsService.AppEvent(this.GetType().Name, dictTrackValues);
            }

        }

        public static void InvokeAndWait(Action action)
        {
            if (MainThread.IsMainThread)
            {
                action();
                return;
            }

            ManualResetEvent evt = new ManualResetEvent(false);
            MainThread.BeginInvokeOnMainThread(() =>
            {
                action();
                evt.Set();
            });
            evt.WaitOne();
        }

        public static void InvokeAndWait(Task action)
        {
            if (MainThread.IsMainThread)
            {
                action.Wait();
                return;
            }

            ManualResetEvent evt = new ManualResetEvent(false);
            MainThread.BeginInvokeOnMainThread(() =>
            {
                action.Wait();
                evt.Set();
            });
            evt.WaitOne();
        }
    }

    public class CoreViewModel<T> : CoreViewModel
    {
        public T Data { get; set; }
        public virtual void Prepare(T parameter)
        {
            Data = parameter;
        }
    }

    public abstract partial class CoreViewModel
    {
        private TimeSpan backClickInterval = new TimeSpan(0, 0, 0, 0, 600);
        private DateTime lastBackdoorClick;
        private int backdoorClickCount = 0;
        private int requiredBackdoorClicks = 5;
        public string DollarSign { get; set; } = "$";

        protected static bool BackdoorEnabled = false;

        [JsonIgnore]
        public ICommand BackdoorCommand { get => GetCommand(tryBackdoor); }
        [JsonIgnore]
       // public ICommand ToggleMenuCommand => GetCommand(toggleMenu);

     
        public bool ShowVersion { get => GetProperty<bool>(); set => SetProperty(value); }


        //void toggleMenu()
        //{
        //    FLIP.App.Instance.ToggleMenu();
        //}

       
        async void tryBackdoor()
        {
            ShowVersion = true;
            await Task.Yield();
            await Task.Delay(new TimeSpan(0, 0, 2));

            ShowVersion = false;
            if (DateTime.Now - lastBackdoorClick <= backClickInterval)
            {
                backdoorClickCount++;
            }
            else
            {
                backdoorClickCount = 1;
            }
            lastBackdoorClick = DateTime.Now;

            if (backdoorClickCount >= requiredBackdoorClicks)
            {
                Logger.Trace($"{backdoorClickCount} clicks");
                openBackDoor();
            }
        }


        protected virtual void openBackDoor()
        {
            //if (BackdoorEnabled)
            //{
            //    App.Instance.CloseMenu();
            //    NavService.Navigate<LogViewModel>();
            //}
        }
    }

    public class SimpleViewModel : INotifyPropertyChanged
    {
        #region Property Get/Set
        public bool IsCryptoStatusEnabled { get; set; } = false;
        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [JsonIgnore]
        public Dictionary<string, object> propertyDictionary = new Dictionary<string, object>();
        protected void SetProperty<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            //Debug.WriteLine($"Setting property : {propertyName}");
            if (propertyDictionary.ContainsKey(propertyName))
            {
                try
                {
                    T oldValue = (T)propertyDictionary[propertyName];

                    if (newValue == null && oldValue == null)
                    {
                        return;
                    }

                    if (newValue != null && newValue.Equals(oldValue))
                        return;
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex);
                }

                propertyDictionary[propertyName] = newValue;
                RaisePropertyChanged(propertyName);
            }
            else
            {
                propertyDictionary.Add(propertyName, newValue);
                RaisePropertyChanged(propertyName);
            }
        }

        protected void SetProperty<TProp>(TProp newValue, Action<TProp, TProp> onChange, [CallerMemberName] string propertyName = null)
        {
            //Debug.WriteLine($"Setting property : {propertyName}");
            if (propertyDictionary.ContainsKey(propertyName))
            {
                TProp oldValue = (TProp)propertyDictionary[propertyName];

                if ((newValue == null && oldValue == null) ||
                    (newValue != null && oldValue != null && newValue.Equals(oldValue)))
                    return;

                propertyDictionary[propertyName] = newValue;
                onChange(oldValue, newValue);
                RaisePropertyChanged(propertyName);
            }
            else
            {
                propertyDictionary.Add(propertyName, newValue);
                onChange(default(TProp), newValue);
                RaisePropertyChanged(propertyName);
            }
        }

        /// <summary>
        /// JUST SET THE BACKING STORE. Do not trigger "changed" or OnChange
        /// </summary>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="newValue"></param>
        /// <param name="propertyName"></param>
        protected void SetPropertyQuiet<TProp>(TProp newValue, [CallerMemberName] string propertyName = null)
        {
            //Debug.WriteLine($"Setting property : {propertyName}");
            if (propertyDictionary.ContainsKey(propertyName))
            {
                TProp oldValue = (TProp)propertyDictionary[propertyName];

                if ((newValue == null && oldValue == null) ||
                    (newValue != null && oldValue != null && newValue.Equals(oldValue)))
                    return;

                propertyDictionary[propertyName] = newValue;
            }
            else
            {
                propertyDictionary.Add(propertyName, newValue);
            }
        }

        protected T GetProperty<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyDictionary.ContainsKey(propertyName))
            {
                return (T)propertyDictionary[propertyName];
            }

            return default(T);
        }

        //protected string GetCountryCode<T>([CallerMemberName] string countryId = null)
        //{
        //    string countryCode = string.Empty;
        //    if (!string.IsNullOrEmpty(countryId))
        //    {
        //        Constants.eDefaultCountry defaultCountry = (Constants.eDefaultCountry)Convert.ToUInt32(countryId);
        //        countryCode = defaultCountry.GetDescription();

        //    }
        //    return countryCode;
        //}

        #endregion

        #region Commands
        [JsonIgnore]
        protected Dictionary<string, Command> commands = new Dictionary<string, Command>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected Command GetCommand(Action action, [CallerMemberName] string propertyName = null)
        {
            if (!commands.ContainsKey(propertyName))
            {
                commands.Add(propertyName, new Command(action));
            }
            return commands[propertyName];
        }

        protected Command GetCommand(Action action, Func<bool> canExecute, [CallerMemberName] string propertyName = null)
        {
            if (!commands.ContainsKey(propertyName))
            {
                var cmd = new Command(action, canExecute);
                commands.Add(propertyName, cmd);
            }
            return commands[propertyName];
        }

        protected Command GetCommand<T>(Action<T> action, [CallerMemberName] string propertyName = null)
        {
            if (!commands.ContainsKey(propertyName))
            {
                commands.Add(propertyName, new Command<T>(action));
            }
            return commands[propertyName];
        }
        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSPL.ViewModels
{
    public class SampleCollectionViewModel : Core.ViewModels.CoreViewModel
    {
      //  public ICommand CountinueCommand => GetCommand(onCountinueCommand);
        public List<string> SampleCollectionList { get => GetProperty<List<string>>(); set => SetProperty(value); }
        public SampleCollectionViewModel()
        {
            List<string> values = new List<string>();
            values.Add("one");
            values.Add("two");
            values.Add("THREE");

            SampleCollectionList = values;
        }
    }
}

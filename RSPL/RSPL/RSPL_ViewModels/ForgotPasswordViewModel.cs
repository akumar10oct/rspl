﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace RSPL.ViewModels
{
    public class ForgotPasswordViewModel : Core.ViewModels.CoreViewModel
    {
        public ICommand CountinueCommand => GetCommand(onCountinueCommand);
        public string EmailId { get => GetProperty<string>(); set => SetProperty(value); }
        public ForgotPasswordViewModel()
        {
           
        }
      
        public async void onCountinueCommand()
        {
            try
            {
                await PopupNavigation.Instance.PushAsync(new Views.ForgotPasswordVerifyPopup());
            }
          catch(Exception ex)
            {
                string error = ex.Message;
            }
        }
        private async void closePopup()
        {
            await PopupNavigation.Instance.PopAsync();

        }
    }
}

﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace RSPL.ViewModels
{
    public class NewPasswordViewModel : Core.ViewModels.CoreViewModel
    {
        public ICommand ResetCommand => GetCommand(onResetCommand);
        public string Password { get => GetProperty<string>(); set => SetProperty(value); }
        public NewPasswordViewModel()
        {

        }
        public async void onResetCommand()
        {
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Core;
using Core.Helpers;
using Core.Services;
using Core.ViewModels;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace RSPL.ViewModels
{
    public class MenuViewModel : CoreViewModel
    {
        public MenuViewModel()
        {
           
        }
        public ICommand LogoutCommand => GetCommand(performLogout);
     

       
       
        public override void ViewAppearing()
        {
            base.ViewAppearing();
           
        }

        //async void openOrders()
        //    {
        //        App.Instance.CloseMenu();
        //        await NavigationService.Instance.Navigate<OrdersViewModel>();
        //    }
        //    async void openAccount()
        //    {
        //        App.Instance.CloseMenu();

        //        await NavigationService.Instance.Navigate<AccountViewModel>();
        //    }
        //    async void openBanking()
        //    {
        //        App.Instance.CloseMenu();

        //        if (App.Instance.User.IsPending)
        //        {
        //            await Dialogs.AlertAsync("Your account is still awaiting approval.");
        //            return;
        //        }
        //        if (App.Instance.User.TradingAccountTypeId > 4 && App.Instance.User.TradingAccountTypeId < 9)
        //        {
        //            await NavigationService.Instance.Navigate<RetirementBankingViewModel>();
        //        }
        //        else if (App.Instance.User.TradingAccountTypeId == 9)
        //        {
        //            await PopupNavigation.Instance.PushAsync(new Views.TextMessagePopup(Constants.CustodianText));
        //            return;
        //        }
        //        else
        //        {

        //            // if (App.Instance.User.NeedsLinkedAccount())
        //            // {
        //            //   await NavService.Navigate<AddNewAccountViewModel>();
        //            // return;
        //            //  }
        //            if (App.Instance.User.NeedsFunding())
        //            {
        //                TransferData data = new TransferData() { Direction = TransferDirection.FromBankToFlip };
        //                await NavigationService.Instance.Navigate<LinkedAcctsViewModel, TransferData>(data);
        //                return;
        //            }
        //            // just open it normally, yay
        //            TransferData Bankingdata = new TransferData() { Direction = TransferDirection.FromBankToFlip };
        //            await NavigationService.Instance.NavigateModal<LinkedAcctsViewModel, TransferData>(null);
        //        }
        //        //await NavigationService.Instance.NavigateModal<BankingViewModel>();
        //    }
        //    async void openHistory()
        //    {
        //        App.Instance.CloseMenu();
        //        await NavigationService.Instance.Navigate<HistoryViewModel, string>(null);
        //    }

        //    async void openSupport()
        //    {
        //        //if (App.AppLinkInfo.HasValue())
        //        //{
        //        //    await Dialogs.AlertAsync(App.AppLinkInfo);
        //        //}
        //        App.Instance.CloseMenu();
        //        await NavigationService.Instance.Navigate<SupportViewModel>();
        //    }

        //    async void openSearchStocks()
        //    {
        //        App.Instance.CloseMenu();
        //        await NavigationService.Instance.Navigate<SearchStockViewModel, bool>(true);
        //    }
        //    async void openPartner()
        //    {
        //        // if already a partner, show the partner screen!
        //        if (IsPartner)
        //        {
        //            // get partner data FIRST
        //            using (var dlg = Dialogs.Loading("Loading..."))
        //            {
        //                await Task.Yield();
        //                var response = await ApiClient.Instance.GetPartnerInformation(UserId);
        //                if (response.Success)
        //                {
        //                    await NavigationService.Instance.Navigate<PartnerViewModel, PartnerResponse>(response.Data);
        //                }
        //                else
        //                {
        //                    dlg.Hide();
        //                    await Task.Yield();
        //                    await Dialogs.AlertAsync("Failed to get partner information. Please check your internet connection and try again.");
        //                }
        //            }
        //        }
        //        else
        //            await NavigationService.Instance.Navigate<PartnerRequestViewModel>();

        //        IsPartner = !IsPartner;
        //    }

        //    async void selectProfilePic()
        //    {
        //        List<string> actions = new List<string>();
        //        if (HasProfilePic)
        //        {
        //            actions.Add(Constants.RemovePhoto);
        //        }
        //        actions.Add(Constants.SelectPhoto);
        //        actions.Add(Constants.TakePhoto);

        //        var action = await Dialogs.ActionSheetAsync("Update Profile Pic", "Cancel", null, null, actions.ToArray());

        //        MediaFile pic = null;
        //        switch (action)
        //        {
        //            case Constants.RemovePhoto:
        //                using (var dlg = Dialogs.Loading("Removing Picture..."))
        //                {
        //                    await Task.Yield();
        //                    var response = await ApiClient.Instance.DeleteAvatar(UserId);
        //                    if (response.Success)
        //                    {
        //                        HasProfilePic = false;
        //                        MissingProfilePic = true;
        //                        ProfilePicUrl = null;
        //                    }
        //                }
        //                break;
        //            case Constants.TakePhoto:
        //                if (await CheckCameraPermission())
        //                {
        //                    StoreCameraMediaOptions options = new StoreCameraMediaOptions()
        //                    {
        //                        AllowCropping = true,
        //                        DefaultCamera = CameraDevice.Rear,
        //                        SaveToAlbum = false,
        //                        PhotoSize = PhotoSize.MaxWidthHeight,
        //                        MaxWidthHeight = 300
        //                    };
        //                    pic = await CrossMedia.Current.TakePhotoAsync(options);
        //                }
        //                break;
        //            case Constants.SelectPhoto:
        //                if (await CheckPhotoPermission())
        //                {
        //                    PickMediaOptions options = new PickMediaOptions()
        //                    {
        //                        PhotoSize = PhotoSize.MaxWidthHeight,
        //                        MaxWidthHeight = 300
        //                    };
        //                    pic = await CrossMedia.Current.PickPhotoAsync(options);
        //                }
        //                break;
        //        }

        //        if (pic != null)
        //        {
        //            using (var dlg = Dialogs.Loading("Uploading..."))
        //            {
        //                var image = pic.GetStreamWithImageRotatedForExternalStorage();
        //                image.Seek(0, System.IO.SeekOrigin.Begin);
        //                using (MemoryStream ms = new MemoryStream())
        //                {
        //                    await image.CopyToAsync(ms);
        //                    ms.Seek(0, SeekOrigin.Begin);

        //                    var response = await ApiClient.Instance.UploadPhoto(UserId, ms);
        //                    Debug.WriteLine(response);
        //                    if (response.Success && response.Data.Message.Equals("Success", StringComparison.OrdinalIgnoreCase))
        //                    {
        //                        HasProfilePic = true;
        //                        User.HasProfilePic = true;
        //                        MissingProfilePic = false;
        //                        SetProfilePic();
        //                        AppAnalyticsServices.AppEvent("Photo_Uploaded", (IDictionary<string, string>)null);
        //                        AppFacebookAnalyticsService.AppEvent("Photo_Uploaded", (IDictionary<string, string>)null);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    void SetProfilePic()
        //    {
        //        ProfilePicUrl = null;

        //        string url = $"{ApiClient.Instance.BaseUrl}/api/account/users/{UserId}/avatar";
        //        ProfilePicUrl = new UriImageSource()
        //        {
        //            Uri = new Uri(url),
        //            CachingEnabled = false,

        //        };
        //    }

        [Obsolete]
        async Task<bool> CheckPhotoPermission()
        {
            var photoStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Photos);
            //var mediaStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.MediaLibrary);
            var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

            if (photoStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
            {
                var permissions = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Photos, Permission.Storage);
                photoStatus = permissions[Permission.Photos];
                //mediaStatus = permissions[Permission.MediaLibrary];
                storageStatus = permissions[Permission.Storage];
            }

            //&& mediaStatus == PermissionStatus.Granted 
            if (photoStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
            {
                return true;
            }
            else
            {
                return false;
                //On iOS you may want to send your user to the settings screen.
            }

        }

        [Obsolete]
        async Task<bool> CheckCameraPermission()
        {
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Camera);

            if (cameraStatus != PermissionStatus.Granted)
            {
                var permissions = await CrossPermissions.Current.RequestPermissionsAsync(Plugin.Permissions.Abstractions.Permission.Camera);
                cameraStatus = permissions[Permission.Camera];
            }

            if (cameraStatus == PermissionStatus.Granted)
            {
                return true;
            }
            else
            {
               
                return false;
                //On iOS you may want to send your user to the settings screen.
            }

        }


        private async void performLogout()
        {
            try
            {
                await NavService.CloseModal();
                App.Instance.SetLoginPage();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

        }

    }
}

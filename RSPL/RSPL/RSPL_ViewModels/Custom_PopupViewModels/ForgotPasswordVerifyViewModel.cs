﻿using Core;
using Core.ViewModels;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace RSPL.ViewModels
{
    public class ForgotPasswordVerifyViewModel : CoreViewModel
    {
        public ICommand CountinueCommand => GetCommand(onCountinueCommand);
        public string EmailId { get => GetProperty<string>(); set => SetProperty(value); }
        public async void onCountinueCommand()
        {
            try
            {
                await PopupNavigation.Instance.PopAsync();
                await NavigationService.Instance.Navigate<NewPasswordViewModel>();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }
        public ForgotPasswordVerifyViewModel()
        {

        }
    }
}

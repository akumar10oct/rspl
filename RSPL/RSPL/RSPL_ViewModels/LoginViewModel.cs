﻿using Core.ViewModels;
using System.Windows.Input;

namespace RSPL.ViewModels
{
    public class LoginViewModel :  CoreViewModel
    {
        public ICommand SignInCommand => GetCommand(onSignInCommand);
        public ICommand ForgotPasswordCommand => GetCommand(onForgotPasswordCommand);
        
        public string EmailId { get => GetProperty<string>(); set => SetProperty(value); }
        public string Password { get => GetProperty<string>(); set => SetProperty(value); }
        public LoginViewModel()
        {

        }
        public void onSignInCommand()
        {
            App.Instance.ShowMainPage();
        }
        public async void onForgotPasswordCommand()
        {
            await NavService.Navigate<ForgotPasswordViewModel>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Helpers;
using Core.Logging;
using Core.ViewModels;
using RSPL.Helpers;
using RSPL.Services;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace Core.Views
{
    public partial class CorePage : ContentPage, IStatusBarPage
    {
        public bool GoodToRender = false;
        protected double KeyboardOffset = 0;
        private bool handlekbAction = false;
        protected bool HandleKeyboardAction
        {
            get
            {
                return handlekbAction;
            }
            set
            {
                handlekbAction = value;
            }
        }
        protected View FloatView = null;
        double keyboardHeight = 0;
        protected Layout<View> entryContainer;

        Animation animation = null;


        public enum Orientation
        {
            Landscape,
            Portriat,
        }

        public bool RestrictOrientation { get; protected set; } = false;
        public bool IgnoreDevicePadding { get; protected set; } = false;
        public static Thickness SafeInsets;
        public static bool HasSafeInsets = false;
        private Orientation pageOrientation;

        public static Orientation DeviceOrientation { get; set; } = Orientation.Portriat;   // making assumptions here.
        public Orientation PageOrientation { get; set; } = Orientation.Portriat;

        public StatusBarStyle StatusBarStyle => getStatusBarStyle();

        protected virtual StatusBarStyle getStatusBarStyle()
        {
            return StatusBarStyle.Dark;
        }

        public CorePage()
        {
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //if (HasSafeInsets && !IgnoreDevicePadding)
            //{
            //    // This hokey shit is here because of some timing crap. probably with xamarin.
            //    Task.Run(async () =>
            //    {
            //        await Task.Delay(10);
            //        MainThread.BeginInvokeOnMainThread(() => {
            //            //this.Padding = SafeInsets;
            //        });
            //        await Task.Delay(150);
            //        MainThread.BeginInvokeOnMainThread(() => {
            //            this.Padding = SafeInsets;
            //        });
            //    });

            //}
            On<iOS>().SetUseSafeArea(true);
            KeyboardOffset = Dev.OnPlatformIdiom<double>(0, 0, -30, -30);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GoodToRender = true;
            HandleDevicePadding();
        }

        public virtual void HandleDevicePadding()
        {
            if (GoodToRender && !IgnoreDevicePadding)
            {
                //Thickness safeInsets = On<Xamarin.Forms.PlatformConfiguration.iOS>().SafeAreaInsets();
                ////this.Padding = safeInsets;
                //SafeInsets = safeInsets;
                //HasSafeInsets = true;
            }
        }

        protected virtual void OnOrientationChanged(Orientation orientation)
        {
            Logger.Trace("Device Orientation changed to " + orientation);
            HandleDevicePadding();
            if (PageOrientation != orientation)
            {
                if (this.BindingContext is CoreViewModel coreVM)
                {
                    coreVM.DeviceOrientation = orientation;
                }
            }
            //DeviceOrientation = orientation;
        }

        //protected override void OnSizeAllocated(double width, double height)
        //{
        //    base.OnSizeAllocated(width, height);
        //    Orientation orientation = width > height ? Orientation.Landscape : Orientation.Portriat;
        //    if (DeviceOrientation != orientation)
        //    {
        //        DeviceOrientation = orientation;
        //    }
        //    if (pageOrientation != orientation)
        //    {
        //        pageOrientation = orientation;
        //        OnOrientationChanged(orientation);
        //    }
        //}

        public virtual void OnKeyboardStateChange(KeyboardState kbState, double height)
        {
            Debug.WriteLine($"Keyboard state changed : {kbState} | {height}");
            if (HandleKeyboardAction)
            {
                OnHandleKeyboard(kbState, height);
            }
        }

        public virtual void OnHandleKeyboard(KeyboardState kbState, double height)
        {
            if (kbState == KeyboardState.Closed && keyboardHeight > 0)
            {
                // annimate closed
                animation = new Animation(HandleAnimation, keyboardHeight, 0, Easing.Linear, AnimationComplete);
                animation.Commit(this, "animation");

            }
            else if (kbState == KeyboardState.Open)
            {
                // animate open
                animation = new Animation(HandleAnimation, keyboardHeight, height + KeyboardOffset, Easing.Linear, AnimationComplete);
                animation.Commit(this, "animation");

            }
        }


        private void AnimationComplete()
        {
            Debug.WriteLine("Float Animation complete");
            FloatView.Margin = new Thickness(15, 15, 20, keyboardHeight + 40);
            animation = null;
        }

        void HandleAnimation(double obj)
        {
            //Debug.WriteLine($"Float Animation In progress {obj}");
            keyboardHeight = obj;
            FloatView.Margin = new Thickness(15, 15, 20, keyboardHeight + 40);
        }


        public void SetupInputActions()
        {
            Command cmd = new Command(HandleInputKeyboardAction);
            entryContainer.Children.ToList().ForEach(x =>
            {
                if (x is Xamarin.Forms.Entry entry)
                {
                    var nextView = GetNextEntryOrButton(entry);
                    if (nextView != null)
                    {
                        entry.ReturnCommand = cmd;
                        entry.ReturnCommandParameter = nextView;
                    }
                }
                if (x is Xamarin.Forms.Picker picker)
                {
                    var nextView = GetNextEntryOrButton(picker);
                    if (nextView != null)
                    {
                        picker.Unfocused += (sender, e) =>
                        {
                            nextView.HandleFocus();
                        };
                    }
                }
            });
        }

        public enum ComponentType
        {
            None,
            Entry,
            Button,
            Picker
        }

        public class NextComponent
        {
            public ComponentType CompType { get; set; }
            public View Component { get; set; }
            public void HandleFocus()
            {
                switch (CompType)
                {
                    case ComponentType.Button:
                        (Component as Button).Command?.Execute(null);
                        break;
                    case ComponentType.Entry:
                        (Component as Xamarin.Forms.Entry).Focus();
                        break;
                    case ComponentType.Picker:
                        (Component as Xamarin.Forms.Picker).Focus();
                        break;
                }
            }
        }

        void HandleInputKeyboardAction(object obj)
        {
            var comp = obj as NextComponent;
            comp.HandleFocus();
        }

        protected NextComponent GetNextEntryOrButton(Xamarin.Forms.Entry entry)
        {
            var index = entryContainer.Children.IndexOf(entry);
            if (index >= 0)
            {
                var next = index + 1;
                while (next < entryContainer.Children.Count)
                {
                    var nextView = entryContainer.Children[next];
                    if (nextView is Frame frame)
                    {
                        // check contents of frame?
                        nextView = frame.Content;
                    }
                    if (!nextView.InputTransparent)
                    {
                        if (nextView is Xamarin.Forms.Entry)
                        {
                            entry.ReturnType = ReturnType.Next;
                            return new NextComponent() { Component = nextView, CompType = ComponentType.Entry };
                        }
                        if (nextView is Xamarin.Forms.Button)
                        {
                            entry.ReturnType = ReturnType.Go;
                            return new NextComponent() { Component = nextView, CompType = ComponentType.Button };
                        }
                        if (nextView is Xamarin.Forms.Picker)
                        {
                            entry.ReturnType = ReturnType.Next;
                            return new NextComponent() { Component = nextView, CompType = ComponentType.Picker };
                        }
                    }
                    next++;
                }
            }

            return null;
        }

        protected NextComponent GetNextEntryOrButton(Xamarin.Forms.Picker picker)
        {
            var index = entryContainer.Children.IndexOf(picker);
            if (index >= 0)
            {
                var next = index + 1;
                while (next < entryContainer.Children.Count)
                {
                    var nextView = entryContainer.Children[next];
                    if (nextView is Frame frame)
                    {
                        // check contents of frame?
                        nextView = frame.Content;
                    }
                    if (nextView is Xamarin.Forms.Entry)
                    {
                        return new NextComponent() { Component = nextView, CompType = ComponentType.Entry };
                    }
                    if (nextView is Xamarin.Forms.Button)
                    {
                        return new NextComponent() { Component = nextView, CompType = ComponentType.Button };
                    }
                    if (nextView is Xamarin.Forms.Picker)
                    {
                        return new NextComponent() { Component = nextView, CompType = ComponentType.Picker };
                    }
                    next++;
                }
            }

            return null;
        }

        public bool first = true;

        public void WatchListText_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var obj = sender as View;
            if (e.PropertyName == "IsVisible")
            {
                if (obj.IsVisible)
                {
                    if (first)
                    {
                        first = false;
                        return;
                    }
                    Debug.WriteLine("Fade in/ out text?");
                    obj.FadeTo(1.0);
                    Task.Run(async () =>
                    {
                        await Task.Delay(3000);
                        MainThread.BeginInvokeOnMainThread(() => {
                            obj.FadeTo(0.0);
                        });
                    });

                }
                // fade in / out text

            }
        }

        public static BindableProperty Prop<T, O>([CallerMemberName] string name = null)
        {
            //System.Diagnostics.Debug.WriteLine("*****************************************************");
            //System.Diagnostics.Debug.WriteLine(name);
            name = name.Replace("Property", "");
            //System.Diagnostics.Debug.WriteLine(name);
            //System.Diagnostics.Debug.WriteLine("*****************************************************");
            return BindableProperty.Create(name, typeof(T), typeof(O), default(T), BindingMode.TwoWay);
        }

        public static BindableProperty Prop<T, O>(BindableProperty.BindingPropertyChangedDelegate onChanged, [CallerMemberName] string name = null)
        {
            name = name.Replace("Property", "");
            return BindableProperty.Create(name, typeof(T), typeof(O), default(T), BindingMode.TwoWay, propertyChanged: onChanged);
        }

        public static BindableProperty PropOneWay<T, O>([CallerMemberName] string name = null)
        {
            name = name.Replace("Property", "");
            return BindableProperty.Create(name, typeof(T), typeof(O), default(T), BindingMode.OneWay);
        }

        public static BindableProperty PropOneWay<T, O>(BindableProperty.BindingPropertyChangedDelegate onChanged, [CallerMemberName] string name = null)
        {
            name = name.Replace("Property", "");
            return BindableProperty.Create(name, typeof(T), typeof(O), default(T), BindingMode.OneWay, propertyChanged: onChanged);
        }

        public static BindableProperty PropD<T, O>(T defaultValue, [CallerMemberName] string name = null)
        {
            name = name.Replace("Property", "");
            return BindableProperty.Create(name, typeof(T), typeof(O), defaultValue, BindingMode.OneWay);
        }

        public static BindableProperty PropD<T, O>(T defaultValue, BindableProperty.BindingPropertyChangedDelegate onChanged, [CallerMemberName] string name = null)
        {
            name = name.Replace("Property", "");
            return BindableProperty.Create(name, typeof(T), typeof(O), defaultValue, BindingMode.OneWay, propertyChanged: onChanged);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using Core.Views;
using Xamarin.Forms;

namespace RSPL.Views
{
    public partial class MenuPage : CorePage
    {
        public MenuPage()
        {
            InitializeComponent();
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, true);
        }
    }
}

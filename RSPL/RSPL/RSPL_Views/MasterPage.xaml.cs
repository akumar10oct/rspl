﻿using System;
using Core;
using Xamarin.Forms;

namespace RSPL.Views
{
    public partial class MasterPage : MasterDetailPage, IMasterDetail
    {
        Page startupPage;
        public MasterPage()
        {
            InitializeComponent();

            //this.Master
            NavigationPage.SetHasNavigationBar(this, false);

            this.MasterBehavior = MasterBehavior.Popover;

            this.Master = NavigationService.Instance.CreatePage<ViewModels.MenuViewModel>();
            this.Master.Title = "Menu";
           
                startupPage = NavigationService.Instance.CreatePage<ViewModels.SampleCollectionViewModel>();
          
            this.IsGestureEnabled = false;

            NavigationPage nav = new NavigationPage(startupPage);
            this.Detail = nav;

            this.IsPresentedChanged += MasterPage_IsPresentedChanged;

            App.Instance.MasterPage = this;

        }



        private void MasterPage_IsPresentedChanged(object sender, EventArgs e)
        {
            App.Instance.TriggerToggled(IsPresented);
        }

        public void HideMenu()
        {
            this.IsPresented = false;
        }

        public void ShowMenu()
        {

            if (this.Master.BindingContext is Core.ViewModels.CoreViewModel vm)
            {
                vm.ViewAppearing();
            }
            this.IsPresented = true;
        }
    }
}

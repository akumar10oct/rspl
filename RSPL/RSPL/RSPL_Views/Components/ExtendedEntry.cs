﻿using System;
using Core.Views;
using Xamarin.Forms;

namespace RSPL.Views
{

    /// <summary>
    /// An extended entry control that allows the Font and text X alignment to be set
    /// </summary>
    public class ExtendedEntry : Entry
    {
        public ExtendedEntry()
        {
            this.Unfocused += Handle_Unfocus;
            //  this.HeightRequest = 40;
            this.FontSize = 12;
           // this.CharacterSpacing = 2;
        }
        public static readonly BindableProperty ShowDoneProperty = CorePage.Prop<bool, ExtendedEntry>();
        public bool ShowDone
        {
            get => (bool)GetValue(ShowDoneProperty);
            set => SetValue(ShowDoneProperty, value);
        }
        private void Handle_Unfocus(object sender, FocusEventArgs e)
        {
            if (ShowDone)
            {
                ReturnCommand?.Execute(ReturnCommandParameter);
            }
        }
        public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(nameof(BorderColor),typeof(Color), typeof(ExtendedEntry), Color.Gray);
        // Gets or sets BorderColor value  
        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }

        public static readonly BindableProperty BorderWidthProperty =
        BindableProperty.Create(nameof(BorderWidth), typeof(int),
            typeof(ExtendedEntry), Device.OnPlatform<int>(1, 2, 2));
        // Gets or sets BorderWidth value  
        public int BorderWidth
        {
            get => (int)GetValue(BorderWidthProperty);
            set => SetValue(BorderWidthProperty, value);
        }
        public static readonly BindableProperty CornerRadiusProperty =
        BindableProperty.Create(nameof(CornerRadius),
            typeof(double), typeof(ExtendedEntry), Device.OnPlatform<double>(6, 7, 7));
        // Gets or sets CornerRadius value  
        public double CornerRadius
        {
            get => (double)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }
        public static readonly BindableProperty IsCurvedCornersEnabledProperty =
        BindableProperty.Create(nameof(IsCurvedCornersEnabled),
            typeof(bool), typeof(ExtendedEntry), true);
        // Gets or sets IsCurvedCornersEnabled value  
        public bool IsCurvedCornersEnabled
        {
            get => (bool)GetValue(IsCurvedCornersEnabledProperty);
            set => SetValue(IsCurvedCornersEnabledProperty, value);
        }


    }
}

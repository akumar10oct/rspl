﻿using Core.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSPL.Views.LoginScreen
{
  
    public partial class EmailEntry : Grid
    {
        public EmailEntry()
        {
            InitializeComponent();
        }
        public static readonly BindableProperty TextProperty = CorePage.Prop<string, EmailEntry>();
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
    }
}
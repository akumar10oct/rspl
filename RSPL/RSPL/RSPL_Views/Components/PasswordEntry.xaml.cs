﻿using Core.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSPL.Views
{
    public partial class PasswordEntry : Grid
    {
        public PasswordEntry()
        {
            InitializeComponent();
        }
        public static readonly BindableProperty TextProperty = CorePage.Prop<string, EmailEntry>();
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
    }
}
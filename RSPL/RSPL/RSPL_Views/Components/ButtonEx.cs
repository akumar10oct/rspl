﻿using System;
using Core.Views;
using Xamarin.Forms;

namespace RSPL.Views
{
    public class ButtonEx : Button
    {
        public ButtonEx()
        {
         
        }

        public static readonly BindableProperty UnderlineProperty = CorePage.Prop<bool, ButtonEx>();
        public bool Underline
        {
            get { return (bool)GetValue(UnderlineProperty); }
            set { SetValue(UnderlineProperty, value); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Core.Logging;
using Core.Views;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RSPL.Views
{
    public class BasePopupPage : PopupPage
    {
        protected Command closeCommand;
        public BasePopupPage()
        {
            closeCommand = new Command(HandleCloseAction);
            this.CloseWhenBackgroundIsClicked = true;
            this.Padding = CorePage.SafeInsets;

            Task.Run(async () =>
            {
                await Task.Delay(10);
                MainThread.BeginInvokeOnMainThread(() => {
                    this.Padding = CorePage.SafeInsets;
                });
                await Task.Delay(150);
                MainThread.BeginInvokeOnMainThread(() => {
                    this.Padding = CorePage.SafeInsets;
                });
            });

        }

        protected static bool IsShowing;
        protected static ManualResetEvent evt;

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.Padding = CorePage.SafeInsets;
        }
        //public static void AskAndBlockThread(BasePopupPage page)
        //{
        //    evt = new ManualResetEvent(false);


        //    MainThread.BeginInvokeOnMainThread(async () =>
        //    {
        //        await PopupNavigation.Instance.PushAsync(page);
        //    });

        //    evt.WaitOne();
        //}

        //public static async Task PromptPopup(BasePopupPage page, bool allowBackgroundClick = false)
        //{
        //    page.CloseWhenBackgroundIsClicked = allowBackgroundClick;
        //    if (MainThread.IsMainThread)
        //    {
        //        await Task.Run(() =>
        //        {
        //            AskAndBlockThread(page);
        //        });
        //    }
        //    else
        //    {
        //        AskAndBlockThread(page);
        //    }
        //}

        public virtual async void HandleCloseAction(object obj)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        #region Property Get/Set
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [JsonIgnore]
        public Dictionary<string, object> propertyDictionary = new Dictionary<string, object>();
        protected void SetProperty<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            //Debug.WriteLine($"Setting property : {propertyName}");
            if (propertyDictionary.ContainsKey(propertyName))
            {
                try
                {
                    T oldValue = (T)propertyDictionary[propertyName];

                    if (newValue == null && oldValue == null)
                    {
                        return;
                    }

                    if (newValue != null && newValue.Equals(oldValue))
                        return;
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex);
                }

                propertyDictionary[propertyName] = newValue;
                RaisePropertyChanged(propertyName);
            }
            else
            {
                propertyDictionary.Add(propertyName, newValue);
                RaisePropertyChanged(propertyName);
            }
        }

        protected void SetProperty<TProp>(TProp newValue, Action<TProp, TProp> onChange, [CallerMemberName] string propertyName = null)
        {
            //Debug.WriteLine($"Setting property : {propertyName}");
            if (propertyDictionary.ContainsKey(propertyName))
            {
                TProp oldValue = (TProp)propertyDictionary[propertyName];

                if ((newValue == null && oldValue == null) ||
                    (newValue != null && oldValue != null && newValue.Equals(oldValue)))
                    return;

                propertyDictionary[propertyName] = newValue;
                onChange(oldValue, newValue);
                RaisePropertyChanged(propertyName);
            }
            else
            {
                propertyDictionary.Add(propertyName, newValue);
                onChange(default(TProp), newValue);
                RaisePropertyChanged(propertyName);
            }
        }

        /// <summary>
        /// JUST SET THE BACKING STORE. Do not trigger "changed" or OnChange
        /// </summary>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="newValue"></param>
        /// <param name="propertyName"></param>
        protected void SetPropertyQuiet<TProp>(TProp newValue, [CallerMemberName] string propertyName = null)
        {
            //Debug.WriteLine($"Setting property : {propertyName}");
            if (propertyDictionary.ContainsKey(propertyName))
            {
                TProp oldValue = (TProp)propertyDictionary[propertyName];

                if ((newValue == null && oldValue == null) ||
                    (newValue != null && oldValue != null && newValue.Equals(oldValue)))
                    return;

                propertyDictionary[propertyName] = newValue;
            }
            else
            {
                propertyDictionary.Add(propertyName, newValue);
            }
        }

        protected T GetProperty<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyDictionary.ContainsKey(propertyName))
            {
                return (T)propertyDictionary[propertyName];
            }

            return default(T);
        }

      
        #endregion

    }
}

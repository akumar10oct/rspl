﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSPL.Views
{
    public partial class ForgotPasswordVerifyPopup : BasePopupPage
    {
        public ForgotPasswordVerifyPopup()
        {
            InitializeComponent();
            this.BindingContext = new ViewModels.ForgotPasswordVerifyViewModel();
        }
    }
}
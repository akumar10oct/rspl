﻿using Core.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSPL.Views
{
    public partial class SampleCollectionPage : CorePage
    {
        public SampleCollectionPage()
        {
            InitializeComponent();
            App.Instance.OnMenuToggled += Instance_OnMenuToggled;
          //  var totalItems = smapleCollection.ItemsSource as List<string>;
          //  smapleCollection.HeightRequest =  6 * 200;
        }
        private void Instance_OnMenuToggled(object sender, bool opened)
        {
            if (opened)
                shadowBox.FadeTo(0.8);
            else
                shadowBox.FadeTo(0);
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.Instance.OnMenuToggled -= Instance_OnMenuToggled;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.Instance.OnMenuToggled -= Instance_OnMenuToggled;
            App.Instance.OnMenuToggled += Instance_OnMenuToggled;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSPL.Views
{
    public static class LoginPageConstant
    {
        public static string WelcomTxt = "Welcome";
        public static string SubTitle = "Please Login to Continue";
        public static string LoginID = "Login ID";
        public static string Password = "Password";
        public static string SignIn = "Sign In";
        
    }
}

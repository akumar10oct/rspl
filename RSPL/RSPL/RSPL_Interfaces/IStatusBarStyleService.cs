﻿using System;
using Xamarin.Forms;

namespace RSPL.Interfaces
{
    public interface IStatusBarStyleService
    {
        void SetStatusBarColor();

    }
}


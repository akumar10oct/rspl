﻿using Core;
using RSPL.ViewModels;
using RSPL.Views;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSPL
{
    public partial class App : Application
    {
        public App()
        {
            Instance = this;
            InitializeComponent();

            MainPage = new MainPage();
        }
        public static App Instance { get; private set; }
        public IMasterDetail MasterPage { get; set; }
        public void TriggerToggled(bool isOpening)
        {
            OnMenuToggled?.Invoke(this, isOpening);
        }
        public event EventHandler<bool> OnMenuToggled;
        protected override void OnStart()
        {
            NavigationService.Init();

            var startPage = NavigationService.Instance.CreatePage<LoginViewModel>();
            this.MainPage = new NavigationPage(startPage);
        }
        public void ShowMainPage()
        {
            NavigationService.Init();
            MainThread.BeginInvokeOnMainThread(() =>
            {
                this.MainPage = NavigationService.Instance.CreatePage<MasterViewModel>();
            });
        }
        protected override void OnSleep()
        {
        }

        public void SetLoginPage()
        {
            try
            {
                NavigationService.Init();

                var startPage = NavigationService.Instance.CreatePage<LoginViewModel>();
                this.MainPage = new NavigationPage(startPage);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }
        public void ToggleMenu()
        {
            if (MasterPage != null)
            {
                if (MasterPage.IsPresented)
                    CloseMenu();
                else
                    OpenMenu();
            }
        }

        public void OpenMenu()
        {
            MasterPage?.ShowMenu();
        }

        public void CloseMenu()
        {
            MasterPage?.HideMenu();

        }
        protected override void OnResume()
        {
        }
    }
}

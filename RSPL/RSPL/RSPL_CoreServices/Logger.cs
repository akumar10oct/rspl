﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Core.Logging
{

	public static class Logger
	{
		private static List<string> _logs;

		public static List<string> Logs { 
			get {
				return _logs ?? (_logs = new List<string> ());
			}
		}

        public static string AppVersion { get; set; }

		public static bool Enabled { get; set; } = true;

		public static void Trace (String Message, [CallerMemberName] String Area = null)
		{
            try
            {
                if (string.IsNullOrWhiteSpace(Message))
                {
                    Write(" NULL TRACE -> " + Area);
                    return;
                }
                if (Area != null)
                {
                    Message = Area + "->" + Message;
                }

                if (Enabled)
                {
                   // DataManager.Instance.AddLogData(new Core.Models.LogEntry(Message));
                }

                Message = String.Format("{0:h:mm:ss.fff} {1}", DateTime.Now, Message);
                Write(Message);
            }
            catch (Exception ex)
            {
                Write($"Failed to write message '{Message}' due to exception {ex.Message}");
            }
		}

		public static void TraceFmt (String Message, params object[] args)
		{
			if (args != null && args.Length > 0) {
				Message = String.Format (Message, args);
			}
			if (Enabled) {
				//DataManager.Instance.AddLogData (new Core.Models.LogEntry (Message));
			}
			Message = String.Format ("{0:h:mm:ss.fff} {1}", DateTime.Now, Message);
			Write (Message);
		}

        public static void LogException (Exception ex, [CallerMemberName] String Area = null)
		{
			String Message = ex.Message;
			LastException = Message;
			ExceptionCount++;

			if (Area != null) {
				Message = Area + "->" + Message;
			}

			//if (Enabled) {
			//	DataManager.Instance.AddLogData (new Core.Models.LogEntry (Message));
			//	DataManager.Instance.AddLogData (new Core.Models.LogEntry (ex.StackTrace));
			//}

			Write ("EXCEPTION: " + Message);
			Write ("STACK TRACE: " + ex.StackTrace);
			Exception inner = ex.InnerException;

			//Xamarin.Insights.Report (ex);

			if (inner != null) {
				LogException (inner, "INNER EX");
			}
		}

		public static String LastException { get; set; }

		public static int ExceptionCount { get; set; }

		public static void Write (String message)
		{
			if (string.IsNullOrWhiteSpace (message)) {
				Write (" NULL WRITE -> ");
				return;
			}

			Logs.Add (message);
			Debug.WriteLine (message);
		}
	}
}


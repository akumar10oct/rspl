﻿using System;
using Core.Logging;

namespace Core.Services
{
    public class LocalStorage
    {
        private LocalStorage()
        {
            Logger.Trace("Init local storage");
            Init();

        }

        private static LocalStorage instance;

        public static LocalStorage Instance
        { get { return instance ?? (instance = new LocalStorage()); } }

        private void Init()
        {

        }

        public void Set(string key, string value)
        {
            Xamarin.Essentials.Preferences.Set(key, value);
        }

        public void Save(string key, string value)
        {
            Xamarin.Essentials.Preferences.Set(key, value);
        }

        public void Delete(string key)
        {
            Xamarin.Essentials.Preferences.Remove(key);
        }

        public string Get(string key, string defaultValue = null)
        {
            return Xamarin.Essentials.Preferences.Get(key, defaultValue);
        }

    }
}

﻿using System;
namespace FLIP.CoreServices
{
    public interface IOrientation
    {
        void SetLandscape();
        void SetPortrait();
    }
}

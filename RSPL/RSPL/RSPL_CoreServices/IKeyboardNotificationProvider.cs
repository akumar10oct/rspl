﻿using System;
namespace RSPL.Services
{
    public enum KeyboardState
    {
        Open,
        Closed
    }

    public interface IKeyboardNotificationProvider
    {
        void KeyboardChangedState(KeyboardState state, double KeyboardHeight);
    }
}

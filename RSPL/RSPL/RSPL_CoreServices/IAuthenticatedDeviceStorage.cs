﻿using System;
using System.Threading.Tasks;

namespace RSPL.Services
{
    public enum SupportedAuthenticationType
    {
        None,
        Touch,
        Face
    }


    public interface IAuthenticatedDeviceStorage
    {
        SupportedAuthenticationType AuthenticationType { get; }

        Task AddData(string key, string value);
        Task<string> GetData(string key);

        void Cancel();
    }
}

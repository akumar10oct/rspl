﻿namespace RSPL.Services
{
    public interface IDeviceOrientationService
    {
        void SetAllAllowed();
        void SetLandscapeOnly();
    }
}

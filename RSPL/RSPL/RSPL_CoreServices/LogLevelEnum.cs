﻿using System;

namespace Core.Models
{
	public enum LogLevelEnum {
		Info,
		Debug,
		Warning,
		Error
	}

}


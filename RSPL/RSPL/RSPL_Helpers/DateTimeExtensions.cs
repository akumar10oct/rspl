﻿using System;
namespace Core.Helpers
{
    public static class DateTimeExtensions
    {
        public static long AsEpoch(this DateTime utcDateTime)
        {
            return (long)(utcDateTime - epoch).TotalSeconds;
        }

        public static DateTime AsDateTime(this long unixTime)
        {
            return epoch.AddSeconds(unixTime);
        }

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static string AsMidLengthString(this DateTime dt)
        {
            int day = dt.Day;

            var suffix = (day % 10 == 1 && day != 11) ? "st"
                : (day % 10 == 2 && day != 12) ? "nd"
                : (day % 10 == 3 && day != 13) ? "rd"
                : "th";

            return dt.ToString("dddd, MMMM d") + suffix;
        }

        public static string AsNiceTime(this TimeSpan ts)
        {
            if (ts.TotalMinutes <= 70.0)
            {
                return $"{(int)ts.TotalMinutes} minute{Plural(ts.TotalMinutes)}" ;
            }
            if (ts.TotalDays > 1.0)
            {
                return $"{(int)ts.TotalDays} day{Plural(ts.TotalDays)}, {ts.Hours} hour{Plural(ts.Hours)}, {ts.Minutes} minute{Plural(ts.Minutes)}";
            }
                
            return $"{ts.Hours} hour{Plural(ts.Hours)}, {ts.Minutes} minute{Plural(ts.Minutes)}";
        }

        private static string Plural(int val)
        {
            return val == 1 ? "" : "s";
        }
        private static string Plural(double val)
        {
            return Plural((int)val);
        }

        public static DateTime FromUTCToNYSELocal(this DateTime dtIn)
        {
            var NYSETZ = TimeZoneInfo.FindSystemTimeZoneById(EST);
            return TimeZoneInfo.ConvertTimeFromUtc(dtIn, NYSETZ);
        }

        // TODO: I don't know if this string will be consistent for all iOS and Android devices.
        private const string EST = "America/New_York";

    }
}

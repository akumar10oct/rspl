﻿using System;

using Xamarin.Forms;

namespace Core
{
    public interface IMasterDetail 
    {
        void ShowMenu();
        void HideMenu();
        bool IsGestureEnabled { get; set; }

        bool IsPresented { get; }

        event EventHandler IsPresentedChanged;
    }
}


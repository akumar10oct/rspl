﻿using System;
using Xamarin.Forms;

namespace Core.Helpers
{
    public static class Dev
    {
        public static bool IsPhone = Device.Idiom == TargetIdiom.Phone;
        public static bool IsTablet = Device.Idiom == TargetIdiom.Tablet;

        public static bool IsAndroid => Device.RuntimePlatform == Device.Android;
        public static bool IsiOS => Device.RuntimePlatform != Device.Android;

        public static T OnIdiom<T>(T phone, T tablet)
        {
            if (DesignMode.IsDesignModeEnabled)
            {
                return phone;
            }

            return Device.Idiom == TargetIdiom.Phone ? phone : tablet;
        }

        public static T OnPlatformIdiom<T>(T iOSphone, T iOStablet, T droidPhone, T droidTablet)
        {
            if (DesignMode.IsDesignModeEnabled)
            {
                return iOSphone;
            }

            return Device.Idiom == TargetIdiom.Phone ? OnPlatform<T>(iOSphone, droidPhone) : OnPlatform<T>(iOStablet, droidTablet);
        }

        public static T OnPlatform<T>(Func<T> iOS = null, Func<T> Android = null, Func<T> WinPhone = null, Func<T> Default = null)
        {
            if (DesignMode.IsDesignModeEnabled)
            {
                iOS();
            }

            iOS = iOS ?? Default;
            Android = Android ?? Default;
            WinPhone = WinPhone ?? Default;
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    return iOS == null ? default(T) : iOS();
                case Device.Android:
                    return Android == null ? default(T) : Android();
                case Device.UWP:
                    return iOS == null ? default(T) : WinPhone();
                default:
                    return Default == null ? default(T) : Default();

            }
        }

        private static void NOP() { }

        public static void OnPlatform(Action iOS = null, Action Android = null, Action WinPhone = null, Action Default = null)
        {
            if (DesignMode.IsDesignModeEnabled)
            {
                iOS();
            }

            iOS = iOS ?? Default;
            Android = Android ?? Default;
            WinPhone = WinPhone ?? Default;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    if (iOS != null) iOS();
                    break;
                case Device.Android:
                    if (Android != null) Android();
                    break;
                case Device.UWP:
                    if (WinPhone != null) WinPhone();
                    break;
                default:
                    if (Default != null) Default();
                    break;
            }
        }

        public static T OnPlatform<T>(T iOS, T Android, T WinPhone)
        {
            if (DesignMode.IsDesignModeEnabled)
            {
                return iOS;
            }

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    return iOS;
                case Device.Android:
                    return Android;
                case Device.UWP:
                    return WinPhone;
                default:
                    return iOS;
            }
        }

        public static T OnPlatform<T>(T iOS, T Android)
        {
            return OnPlatform<T>(iOS, Android, default(T));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Core.Views
{
    public class MaskedBehavior : Behavior<Entry>
    {
        public static readonly BindableProperty MaskProperty = CorePage.PropOneWay<string, MaskedBehavior>(maskChanged);
        public Entry entry;

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

        }

        public string Mask
        {
            get => (string)GetValue(MaskProperty); 
            set => SetValue(MaskProperty, value); 
        }

        private static void maskChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as MaskedBehavior).SetPositions();
            (bindable as MaskedBehavior).UpdateText();
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            entry = bindable;
            bindable.TextChanged += OnEntryTextChanged;
            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnEntryTextChanged;
            entry = null;
            base.OnDetachingFrom(bindable);
        }

        IDictionary<int, char> _positions;

        void SetPositions()
        {
            if (Mask.IsEmpty())
            {
                _positions = null;
                return;
            }

            var list = new Dictionary<int, char>();
            for (var i = 0; i < Mask.Length; i++)
                if (Mask[i] != 'X')
                    list.Add(i, Mask[i]);

            _positions = list;
            UpdateText();
        }

        private void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            UpdateText();
        }

        private void UpdateText()
        {
            if (entry == null || Mask.IsEmpty())
            {
                Debug.WriteLine("Mask or Entry not set");
                return;
            }

            var text = entry.Text;

            if (string.IsNullOrWhiteSpace(text) || _positions == null)
                return;

            if (text.Length > Mask.Length)
            {
                entry.Text = text.Remove(text.Length - 1);
                return;
            }

            foreach (var position in _positions)
                if (text.Length >= position.Key + 1)
                {
                    var value = position.Value.ToString();
                    if (text.Substring(position.Key, 1) != value)
                        text = text.Insert(position.Key, value);
                }

            if (entry.Text != text)
                entry.Text = text;
        }
    }
}
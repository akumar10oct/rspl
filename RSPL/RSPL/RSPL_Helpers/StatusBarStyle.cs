﻿using System;
namespace RSPL.Helpers
{
    public enum StatusBarStyle
    {
        Light,
        Dark
    }

    public interface IStatusBarPage
    {
        StatusBarStyle StatusBarStyle { get; }
    }
}


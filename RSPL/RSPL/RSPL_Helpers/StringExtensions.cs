﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using RSPL.ViewModels;

namespace Core
{
    public static class StringExtensions
    {
        public static bool HasValue(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }

        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static T Copy<T>(this T obj) where T : class
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }

        public static bool IsPOBox(this string str)
        {
            string pattern = @"(?i)\b(?:p(?:ost)?\.?\s*(?:[o0](?:ffice)?)?\.?\s*b(?:[o0]x)?|b[o0]x)";
            Regex rx = new Regex(pattern);
            return rx.IsMatch(str);
        }

        public static bool IsEmail(this string str)
        {
            if (str.IsEmpty())
                return false;
            return str.Contains("@") &&
                str.Length > 3
                && str.Contains(".")
                && !str.EndsWith(".")
                && str.IndexOf("@") > 0
                && str.IndexOf("@") < str.LastIndexOf(".")
                && !str.EndsWith("@");
        }
        public static bool IsVaildLastName(this string str)
        {
            if (str.IsEmpty())
                return false;
            return !str.Contains("@");
        }

        public static bool Contains(this string str, string match, StringComparison comparisonType)
        {
            if (str.IsEmpty())
                return false;

            if (match.IsEmpty())
                return false;

            return str.ToLower().Contains(match.ToLower());
        }
    }

}

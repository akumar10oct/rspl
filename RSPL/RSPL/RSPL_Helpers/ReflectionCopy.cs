﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace RSPL
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CopyPropertyAttribute : Attribute
    {
        public string SourceProperty { get; set; }
        public bool Ignore { get; set; }
    }


    /// <summary>
    /// A static class for reflection type functions
    /// </summary>
    public static class Reflection
    {
        /// <summary>
        /// Extension for 'Object' that copies the properties to a destination object.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        public static bool CopyProperties(this object source, object destination)
        {
            // If any this null throw an exception
            if (source == null || destination == null)
                throw new Exception("Source or/and Destination Objects are null");

            bool dataCopied = false;
            // Getting the Types of the objects
            Type typeDest = destination.GetType();
            Type typeSrc = source.GetType();

            // Iterate the Properties of the source instance and  
            // populate them from their desination counterparts  
            PropertyInfo[] srcProps = typeSrc.GetProperties();
            PropertyInfo[] trgProps = typeDest.GetProperties();

            foreach (PropertyInfo srcProp in srcProps)
            {
                if (!srcProp.CanRead)
                {
                    continue;
                }

                PropertyInfo targetProperty = trgProps.FirstOrDefault(x => x.Name.Equals(srcProp.Name, StringComparison.OrdinalIgnoreCase));
                if (targetProperty != null)
                {
                    CopyProperty(srcProp, targetProperty, source, destination, ref dataCopied);
                }

                var copyProp = trgProps.FirstOrDefault(x => x.CustomAttributes.Any(a => a.AttributeType == typeof(CopyPropertyAttribute)
                                && a.NamedArguments.Any(na => na.MemberName.Equals("SourceProperty", StringComparison.OrdinalIgnoreCase)
                                                        && na.TypedValue.Value.ToString() == srcProp.Name)));
                if (copyProp != null && copyProp != targetProperty)
                {
                    CopyProperty(srcProp, copyProp, source, destination, ref dataCopied);
                }

            }

            return dataCopied;
        }

        private static void CopyProperty(PropertyInfo srcProp, PropertyInfo targetProperty, object source, object destination, ref bool dataCopied)
        {
            if (!targetProperty.CanWrite)
            {
                return;
            }
            if (targetProperty.GetSetMethod(true) != null && targetProperty.GetSetMethod(true).IsPrivate)
            {
                return;
            }
            if ((targetProperty.GetSetMethod().Attributes & MethodAttributes.Static) != 0)
            {
                return;
            }
            if (!targetProperty.PropertyType.IsAssignableFrom(srcProp.PropertyType))
            {
                return;
            }

            var copyProp = targetProperty.GetCustomAttribute<CopyPropertyAttribute>(true);
            if (copyProp != null)
            {
                if (copyProp.Ignore)
                    return;
            }

            object srcValue = srcProp.GetValue(source, null);
            object targetValue = targetProperty.GetValue(destination, null);

            if (srcValue == null && targetValue == null)
                return;

            bool needsCopy = (srcValue == null || targetValue == null)
                || (!srcValue.ToString().Equals(targetValue.ToString()));

            if (!needsCopy)
                return;

            // Passed all tests, lets set the value

            dataCopied = true;
            targetProperty.SetValue(destination, srcProp.GetValue(source, null), null);
        }
    }
}
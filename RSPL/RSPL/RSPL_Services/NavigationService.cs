﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Core.ViewModels;
using System.Reflection;
using Rg.Plugins.Popup.Services;

namespace Core
{
    public class NavigationService
    {
        private NavigationService()
        {
        }

        public static void Init()
        {
            HasModal = false;
            if (Mapping.Count > 0)
            {
                // already initialized
                return;
            }

            var types = Assembly.GetCallingAssembly().GetTypes();
            var pages = types.Where(x => x.IsClass && x.IsPublic && (
                                x.Name.EndsWith("page", StringComparison.OrdinalIgnoreCase) ||
                                x.Name.EndsWith("view", StringComparison.OrdinalIgnoreCase))
                        );
            var vms = types.Where(x => x.IsClass && x.IsPublic && x.Name.EndsWith("viewmodel", StringComparison.OrdinalIgnoreCase));
            Debug.WriteLine($"found {pages.Count()} pages and {vms.Count()} view models");
            foreach (var vm in vms)
            {
                string name = vm.Name.ToLower().Replace("viewmodel", "");
                var page = pages.FirstOrDefault(x => x.Name.Equals(name + "page", StringComparison.OrdinalIgnoreCase));
                if (page != null)
                {
                    Debug.WriteLine($"Mapping {vm.Name} to page {page.Name} ");
                    Mapping.Add(vm, page);
                }
            }

        }

        private void Instance_Popped(object sender, Rg.Plugins.Popup.Events.PopupNavigationEventArgs e)
        {
            // jacob
            var page = CurrentPage;
            if (page != null)
            {
                if (page.BindingContext is CoreViewModel coreVm)
                {
                    coreVm.PopupClosed(e.Page);
                }
            }
        }

        static NavigationService _nav = null;
        public static bool HasModal = false;
        public static NavigationPage ModalNav = null;
        public static NavigationService Instance
        {
            get => _nav ?? (_nav = new NavigationService()); 
        }

        public event EventHandler<NavigateEventArgs> BeforeNavigate;
        public event EventHandler<NavigateEventArgs> AfterNavigate;

        public Task Navigate<T>(T viewModel = null) where T : Core.ViewModels.CoreViewModel
        {
            if (HasModal)
                return NavigateModal<T>(viewModel);

            return InternalNavigate<T, object>(viewModel, null);
        }

        public Task Navigate<T1, T2>(T2 initParam, string navFrom = null) where T1 : Core.ViewModels.CoreViewModel
        {
            if (HasModal)
                return NavigateModal<T1, T2>(null, initParam, navFrom);

            return InternalNavigate<T1, T2>(null, initParam, navFrom);
        }

        public Task Navigate<T1, T2>(T1 viewModel, T2 initParam, string navFrom = null) where T1 : Core.ViewModels.CoreViewModel
        {
            if (HasModal)
                return NavigateModal<T1, T2>(viewModel, initParam, navFrom);

            return InternalNavigate<T1, T2>(viewModel, initParam, navFrom);
        }

        public bool ModalCanceled { get; set; }
        public Task NavigateModal<T1, T2>(T2 startParam, string navFrom = null) where T1 : Core.ViewModels.CoreViewModel
        {
            ModalCanceled = false;
            var page = InternalCreatePage<T1, T2>(null, startParam, navFrom);
            return NavigatePage(page);
        }

        public Task NavigateModal<T1, T2>(T1 viewModel, T2 startParam, string navFrom = null) where T1 : Core.ViewModels.CoreViewModel
        {
            ModalCanceled = false;
            var page = InternalCreatePage<T1, T2>(viewModel, startParam, navFrom);
            return NavigatePage(page);
        }

        public Task NavigatePage(Page page)
        { 
            if (page != null)
            {
                if (HasModal)
                {
                    // push it onto the modal navigation window
                    return ModalNav.PushAsync(page);
                }
                else
                {
                    // create a modal navigation window.
                    var topPage = NavigationService.Instance.NavigationPage.CurrentPage;
                    ModalNav = new NavigationPage(page);

                    ModalNav.Disappearing += ModalNav_Disappearing;
                    HasModal = true;
                    return topPage.Navigation.PushModalAsync(ModalNav);
                }
            }
            else
            {
                return null;
            }
        }

        private async void ModalNav_Disappearing(object sender, EventArgs e)
        {
            if (ModalNav != null)
            {
                try
                {
                    await this.NavigationPage.Navigation.PopModalAsync();
                    ModalNav.Disappearing -= ModalNav_Disappearing;
                    ModalNav = null;
                }
                catch(Exception ex)
                {

                }                
            }
            HasModal = false;
        }

        public Task NavigateModal<T>(T viewModel = null) where T : Core.ViewModels.CoreViewModel
        {
            return NavigateModal<T, object>(viewModel:viewModel, startParam:null);
        }

        public async Task CloseModal()
        {
            if (HasModal && ModalNav != null)
            {
                ModalNav.Disappearing -= ModalNav_Disappearing;
                ModalNav = null;
                await this.NavigationPage.Navigation.PopModalAsync();
                HasModal = false;
            }
        }

        public async Task PopModal()
        {
            if (HasModal && ModalNav != null)
            {
                Debug.WriteLine($"Modal Stack = {ModalNav.Navigation.NavigationStack.Count}");

                if (ModalNav.Navigation.NavigationStack.Count == 1)
                {
                    await CloseModal();
                }
                else
                {
                    await ModalNav.PopAsync();
                }
            }
        }

        public Page CreatePage<T>(T viewModel = null) where T : Core.ViewModels.CoreViewModel
        {
            return InternalCreatePage<T, object>(viewModel, null);
        }

        private Page CreatePage<T, T2>(T viewModel, T2 startParam) where T : Core.ViewModels.CoreViewModel
        {
            return InternalCreatePage<T, T2>(viewModel, startParam);
        }

        private Page InternalCreatePage<T, T2>(T viewModel, T2 startParam, string navFrom = null) where T : Core.ViewModels.CoreViewModel
        {
            try
            {

                if (Mapping.ContainsKey(typeof(T)))
                {
                    var pageType = Mapping[typeof(T)];
                    var page = Activator.CreateInstance(pageType) as Page;

                    var vm = viewModel ?? Activator.CreateInstance<T>() as Core.ViewModels.CoreViewModel;
                    vm.SetNavigatedFrom(navFrom ?? CurrentViewModelName);

                    if (vm is Core.ViewModels.CoreViewModel<T2> vm2)
                    {
                        vm2.Prepare(startParam);
                    }

                    page.BindingContext = vm;

                    page.Appearing += Page_Appearing;
                    page.Disappearing += Page_Disappearing;
                    return page;
                }
                return null;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public string CurrentViewModelName
        {
            get
            {
                return CurrentPage?.BindingContext?.GetType().Name;
            }
        }

        private async Task InternalNavigate<T,T2>(T viewModel, T2 startParam, string navFrom = null) where T : Core.ViewModels.CoreViewModel
        {
            var page = InternalCreatePage<T, T2>(viewModel, startParam, navFrom);
            if (page != null) 
            {
                var vm = page.BindingContext as Core.ViewModels.CoreViewModel;
                // I better have a navigation page, ptherwise I just set MAIN PAGE, right?
                //

                BeforeNavigate?.Invoke(this, new NavigateEventArgs() { ViewModel = vm });

                if (NavigationPage == null)
                {
                    if (page is MasterDetailPage masterPage)
                    {
                        InitMasterDetailPage(masterPage);
                    }

                    if (MainThread.IsMainThread)
                    {
                        Application.Current.MainPage = page;
                    }
                    else
                    {
                        MainThread.BeginInvokeOnMainThread(() =>
                        {
                            Application.Current.MainPage = page;
                        });
                    }
                }
                else
                {
                    Debug.WriteLine("Start Nav");
                    if (MainThread.IsMainThread)
                    {
                        await NavigationPage.PushAsync(page);
                    }
                    else
                    {
                        MainThread.BeginInvokeOnMainThread(async () =>
                        {
                            await NavigationPage.PushAsync(page);
                        });
                    }

                    Debug.WriteLine("End Nav");
                }
                AfterNavigate?.Invoke(this, new NavigateEventArgs() { ViewModel = vm });
                vm.ViewAppeared();
            }
        }

        void Page_Appearing(object sender, EventArgs e)
        {
            //(sender as Page).Appearing -= Page_Appearing;
            ((sender as Page).BindingContext as Core.ViewModels.CoreViewModel)?.ViewAppearing();
        }

        void Page_Disappearing(object sender, EventArgs e)
        {
            //(sender as Page).Disappearing -= Page_Disappearing;
            ((sender as Page).BindingContext as Core.ViewModels.CoreViewModel)?.ViewDisappearing();
        }

        public Page CurrentPage
        {
            get
            {
                Page mainPage = Application.Current.MainPage;
                if (HasModal && this.NavigationPage.Navigation.ModalStack != null && this.NavigationPage.Navigation.ModalStack.Count > 0)
                {
                    mainPage = this.NavigationPage.Navigation.ModalStack.Last();
                }

                if (mainPage is MasterDetailPage)
                {
                    mainPage = (mainPage as MasterDetailPage).Detail;
                }

                if (mainPage is NavigationPage)
                {
                    mainPage = (mainPage as NavigationPage).CurrentPage;
                }

                return mainPage;
            }
        }

        public NavigationPage NavigationPage
        {
            get
            {
                Page mainPage = Application.Current.MainPage;
                if (mainPage is MasterDetailPage)
                {
                    mainPage = (mainPage as MasterDetailPage).Detail;
                }

                if (mainPage is NavigationPage nav)
                {
                    InitNavPage(nav);
                    return nav;
                }
                return null;
            }
        }

        MasterDetailPage _masterPage = null;
        public void InitMasterDetailPage(MasterDetailPage masterPage)
        {
            if (masterPage == null)
                return;

            if (_masterPage != null && _masterPage != masterPage)
            {
                // un-listen!
                masterPage.IsPresentedChanged -= MasterPage_IsPresentedChanged;
            }
            if (_masterPage != null && _masterPage == masterPage)
            {
                return;
            }

            _masterPage = masterPage;
            masterPage.IsPresentedChanged += MasterPage_IsPresentedChanged;
        }

        void MasterPage_IsPresentedChanged(object sender, EventArgs e)
        {
            if (_masterPage.Master.BindingContext is Core.ViewModels.CoreViewModel vm)
            {
                if (_masterPage.IsPresented)
                {
                    //_masterPage.Master.Appearing
                    vm.ViewAppearing();
                    vm.ViewAppeared();
                }
                else
                {
                    vm.ViewDisappearing();
                }
            }
        }

        NavigationPage _navPage = null;
        private void InitNavPage(NavigationPage navPage)
        {
            if (_navPage != null && _navPage != navPage)
            {
                // un-listen!
                navPage.Pushed -= NavPage_Pushed;
                navPage.Popped -= NavPage_Popped;
                navPage.PoppedToRoot -= NavPage_PoppedToRoot;
                //navPage.Disappearing -= NavPage_Disappearing;
                //navPage.Appearing -= NavPage_Appearing;
            }
            if (_navPage != null && _navPage == navPage)
            {
                return;
            }

            _navPage = navPage;
            navPage.Pushed += NavPage_Pushed;
            navPage.Popped += NavPage_Popped;
            navPage.PoppedToRoot += NavPage_PoppedToRoot;
            //navPage.Disappearing += NavPage_Disappearing;
            //navPage.Appearing += NavPage_Appearing;
        }

        //void NavPage_Appearing(object sender, EventArgs e)
        //{
        //    if (sender is Page)
        //    {
        //        Debug.WriteLine("Hey");
        //    }
        //}


        //void NavPage_Disappearing(object sender, EventArgs e)
        //{
        //}

        void NavPage_PoppedToRoot(object sender, NavigationEventArgs e)
        {
            var topPage = _navPage.Navigation.NavigationStack.Last();
            if (topPage == e.Page)
            {
                Debug.WriteLine("I was wrong");
            }

            if (e is PoppedToRootEventArgs args)
            {
                foreach (var page in args.PoppedPages)
                {
                    page.Appearing -= Page_Appearing;
                    page.Disappearing -= Page_Disappearing;
                    if (page.BindingContext is Core.ViewModels.CoreViewModel coreVM)
                    {
                        coreVM.ViewDisappearing();
                    }
                }
            }

            // foreach (var page in e.)

        }

        void NavPage_Popped(object sender, NavigationEventArgs e)
        {
            var topPage = _navPage.Navigation.NavigationStack.Last();
            if (topPage == e.Page)
            {
                // Debug.WriteLine("I was wrong");
                if (topPage.BindingContext is CoreViewModel coreViewModel)
                {
                    coreViewModel.ViewAppeared();
                }
            }

            e.Page.Appearing -= Page_Appearing;
            e.Page.Disappearing -= Page_Disappearing;
            if (e.Page.BindingContext is Core.ViewModels.CoreViewModel coreVM)
            {
                coreVM.ViewDisappearing();
            }
        }

        void NavPage_Pushed(object sender, NavigationEventArgs e)
        {

        }

        public async Task PopAsync()
        {
            // handle events here when we can!

            if (NavigationPage == null)
            {
                Debug.WriteLine("Cannot pop top page!!");
            }
            else
            {
                var stack = this.NavigationPage.Navigation.NavigationStack.ToList();
                if (stack.Count > 1)
                {
                    var lastPage = stack.Last();
                    stack.Reverse();
                    var nextToLast = stack.Skip(1).First();
                    if (nextToLast.BindingContext is CoreViewModel coreViewModel)
                    {
                        // may have to come back to this! 
                        //coreViewModel.ViewAppearing();
                    }
                }

                await NavigationPage.PopAsync();
            }
        }

        public async Task PopToRootAsync()
        {
            // handle events here when we can!

            if (NavigationPage == null)
            {
                Debug.WriteLine("Cannot pop top page!!");
            }
            else
            {
                await NavigationPage.PopToRootAsync();
            }
        }

        public async Task PopToAsync<T>()
        {
            if (NavigationPage != null)
            {
                var list = NavigationPage.Navigation.NavigationStack.ToList();
                var page = list.FirstOrDefault(x => x.BindingContext is T);
                    
                if (page == null)
                {
                    await PopToRootAsync();
                }
                else
                {
                    var popToIndex = list.IndexOf(page);
                    if (popToIndex < list.Count - 1)
                    {
                        var pagesToRemove = list.Skip(popToIndex + 1).ToList();
                        if (pagesToRemove.Count > 0)
                            pagesToRemove.RemoveAt(pagesToRemove.Count - 1);

                        if (pagesToRemove.Count > 0)
                        {
                            foreach (var tempPage in pagesToRemove)
                            {
                                NavigationPage.Navigation.RemovePage(tempPage);
                            }
                        }
                    }
                    await PopAsync();
                }
                
            }
        }

        public static Dictionary<Type, Type> Mapping = new Dictionary<Type, Type>();

        public static void Register<T1, T2>() where T1 : Core.ViewModels.CoreViewModel
        {
            Mapping.Add(typeof(T1), typeof(T2));
        }
    }

    public class NavigateEventArgs : EventArgs
    {
        public object Mode { get; set; }
        public object ViewModel { get; set; }
    }

  //  async void NavigationService_AfterNavigate(object sender, NavigateEventArgs e)
}

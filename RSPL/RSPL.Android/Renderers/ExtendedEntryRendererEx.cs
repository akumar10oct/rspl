﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RSPL.Droid.Renderers;
using RSPL.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ExtendedEntry), typeof(ExtendedEntryRendererEx))]
namespace RSPL.Droid.Renderers
{
    public class ExtendedEntryRendererEx : EntryRenderer
    {
        public Android.Util.DisplayMetrics DisplayMetrics => Context.Resources.DisplayMetrics;
        public float DisplayDensity => DisplayMetrics.Density;

        public ExtendedEntryRendererEx(Context context) : base(context)
        {
        }
        //protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        //{
        //    base.OnElementChanged(e);
        //    if (Control != null)
        //    {
        //        Control.Gravity = GravityFlags.CenterHorizontal;

        //        GradientDrawable gradientDrawable = new GradientDrawable();
        //        gradientDrawable.SetCornerRadius(10f * DisplayDensity);
        //        gradientDrawable.SetColor(Android.Graphics.Color.Transparent);
        //        Control.SetBackground(gradientDrawable);
        //        Control.Gravity = GravityFlags.CenterHorizontal;
        //    }
        //}
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                var shape = new ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
                shape.Paint.Color = Xamarin.Forms.Color.Black.ToAndroid();
                shape.Paint.SetStyle(Paint.Style.Stroke);
                Control.Background = shape;
            }
        }
    }
}
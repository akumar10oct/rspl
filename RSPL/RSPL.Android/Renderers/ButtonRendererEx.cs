﻿using System;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Core.Views;
using RSPL.Droid.Renderers;
using RSPL.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using d = System.Diagnostics.Debug;

[assembly: ExportRenderer(typeof(ButtonEx), typeof(ButtonRendererEx))]

namespace RSPL.Droid.Renderers
{

    public class ButtonRendererEx : ButtonRenderer
    {
        public ButtonRendererEx(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                e.NewElement.PropertyChanged += NewElement_PropertyChanged;
                SetupButton();
            }

        }

        private void NewElement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Xamarin.Forms.Button.FontFamilyProperty.PropertyName)
            {
                SetFontFamily();
            }
        }

        void SetupButton()
        {
            try
            {
                // Control.SetBackgroundResource(FLIP.Droid.Resource.Drawable.check);
                SetFontFamily();

                if (Element is ButtonEx btn)
                {
                    if (btn.Underline)
                    {
                        Control.PaintFlags = Control.PaintFlags | PaintFlags.UnderlineText;
                    }
                }
            }
            catch (Exception ex)
            {
               // MessageBus.Current.SendError(ex.Message);
            }
        }

        private void SetFontFamily()
        {
            if (Element.FontFamily != null && !Element.FontFamily.Contains("sans", StringComparison.OrdinalIgnoreCase))
            {
                // list them out!
                //var all = Context.Assets.List("");
                //foreach (var item in all) d.WriteLine(item);

                if (Context.Assets.List("").Contains(Element.FontFamily))
                {
                    Control.Typeface = Typeface.CreateFromAsset(Context.Assets, Element.FontFamily);
                }
            }
        }
    }
}
